package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@SpringBootApplication
@RestController
public class DemoApplication {  
	  private static final Logger LOGGER = LoggerFactory.getLogger(DemoApplication.class);
	
	public static void main(String[] args) {		
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@RequestMapping(value = "/")
	public String hello() {
		LOGGER.info("test.info");
		
	   return "Hello info Yusuf runner-24";
	}

	@RequestMapping(value = "/err")
	public String err() {
		LOGGER.error("test.error");
		String s = "string";

		try {
			LOGGER.info(s);
			s.charAt(10);			
		} catch (Exception e) {
			LOGGER.error("test error", e.fillInStackTrace() );//TODO: handle exception
		}
		
	   return "Hello error Yusuf runner-24";
	}

	@RequestMapping(value = "/deb")
	public String deb() {
		LOGGER.debug("test.debug");
		
	   return "Hello debug Yusuf runner-24";
	}
}